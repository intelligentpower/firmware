/**
 ******************************************************************************
 * @file    outlet_timers.c
 * @author  Logan Anteau
 * @version V1.0.0
 * @date    03-December-2014
 *
 * @brief   Outlet Timer Functions
 ******************************************************************************
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "outlet_timers.h"
#include "hw_config.h"
#include "outlet_control.h"

timer outlet_timers[OUTLETn][MAX_TIMERS];
char status_string[40];

void init_timers(void)
{
    int i;
    for(i=0; i<OUTLETn; i++)
    {
        int j;
        for(j=0; j<MAX_TIMERS; j++)
        {
            outlet_timers[i][j].weekday = 255;
            outlet_timers[i][j].hour = 255;
            outlet_timers[i][j].minute = 255;
            outlet_timers[i][j].state = OFF;
            outlet_timers[i][j].status = DISABLED;
            outlet_timers[i][j].repeat = false;
            outlet_timers[i][j].last_processed = -1;
        }
    }
}

int set_time(uint8_t hours, uint8_t minutes, uint8_t seconds)
{
    rtc_time t;

    t.hours = hours;
    t.minutes = minutes;
    t.seconds = seconds;
    SetRTCTime(t);
    
    return 0;
}

int set_date(uint8_t weekday, uint8_t month, uint8_t day, uint8_t year)
{
    rtc_date d;

    d.weekday = weekday;
    d.month = month;
    d.day = day;
    d.year = year;
    SetRTCDate(d);

    return 0;
}

int add_timer(uint8_t outlet, uint8_t weekday, uint8_t hours, uint8_t minutes,
        Outlet_State_TypeDef state, timer_status status, bool repeat)
{
    int j;
    for(j=0; j<MAX_TIMERS; j++)
    {
        if(outlet_timers[outlet][j].hour == 255)
        {
            /* Add the timer in the u nused spot */
            outlet_timers[outlet][j].weekday = weekday;
            outlet_timers[outlet][j].hour = hours;
            outlet_timers[outlet][j].minute = minutes;
            outlet_timers[outlet][j].state = state;
            outlet_timers[outlet][j].status = status;
            outlet_timers[outlet][j].repeat = repeat;
            return j; /* Return timer index */
        }
    }
    /* Return -1 if unable to add timer */
    return -1;
}

int update_timer(uint8_t outlet, uint8_t id, uint8_t weekday, uint8_t hours,
    uint8_t minutes, Outlet_State_TypeDef state, timer_status status, bool repeat)
{
    if(outlet_timers[outlet][id].hour != 255)
    {
        /* Add the timer in the unused spot */
        outlet_timers[outlet][id].weekday = weekday;
        outlet_timers[outlet][id].hour = hours;
        outlet_timers[outlet][id].minute = minutes;
        outlet_timers[outlet][id].state = state;
        outlet_timers[outlet][id].status = status;
        outlet_timers[outlet][id].repeat = repeat;
        outlet_timers[outlet][id].last_processed = -1;
        return 0;
    }

    return -1; /* Tried to update an unused timer */
}

int del_timer(uint8_t outlet, uint8_t id)
{
    /* Return 1 to indicate failure to delete timer if indexes are invalid */
    if(outlet > OUTLETn || id > MAX_TIMERS)
        return 1;
    else if(outlet < 0 || id < 0)
        return 1;

    outlet_timers[outlet][id].weekday = 255;
    outlet_timers[outlet][id].hour = 255;
    outlet_timers[outlet][id].minute = 255;
    outlet_timers[outlet][id].state = OFF;
    outlet_timers[outlet][id].status = DISABLED;
    outlet_timers[outlet][id].repeat = false;
    outlet_timers[outlet][id].last_processed = -1;

    return 0;
}

int disable_timer(uint8_t outlet, uint8_t id)
{
    /* Return 1 to indicate failure to disable timer if indexes are invalid */
    if(outlet > OUTLETn || id > MAX_TIMERS)
        return 1;
    else if(outlet < 0 || id < 0)
        return 1;
    else if(outlet_timers[outlet][id].weekday == 255) /* Timer not setup */
        return 2;

    outlet_timers[outlet][id].status = DISABLED;

    return 0;
}

int enable_timer(uint8_t outlet, uint8_t id)
{
    /* Return 1 to indicate failure to enable timer if indexes are invalid */
    if(outlet > OUTLETn || id > MAX_TIMERS)
        return 1;
    else if(outlet < 0 || id < 0)
        return 1;
    else if(outlet_timers[outlet][id].weekday == 255) /* Timer not setup */
        return 2;
        
    outlet_timers[outlet][id].status = ENABLED;

    return 0;
}

void process_timers(void)
{
    rtc_time t;
    t = GetRTCTime();

    rtc_date d;
    d = GetRTCDate();

    /* Get the bitstring of the current weekday */
    uint8_t weekday = 0x01 << (d.weekday - 1);

    int i;
    for(i=0; i<OUTLETn; i++)
    {
        int j;
        for(j=0; j<MAX_TIMERS; j++)
        {
            // if(outlet_timers[i][j].status == ENABLED)
            // {
            //     sprintf(status_string,
            //         "%u,%u,%u,%u,%u,%u",
            //         j, i, outlet_timers[i][j].weekday, outlet_timers[i][j].hour,
            //         outlet_timers[i][j].minute, outlet_timers[i][j].repeat);
            //     Spark.publish("timer", status_string);
            //     Delay(1000);
            // }


            if( outlet_timers[i][j].status == ENABLED &&
                weekday & outlet_timers[i][j].weekday &&
                t.hours == outlet_timers[i][j].hour &&
                t.minutes == outlet_timers[i][j].minute &&
                outlet_timers[i][j].last_processed != d.weekday &&
                outlet_states[i] != outlet_timers[i][j].state)
            {
                outlet_timers[i][j].last_processed = d.weekday;
                if(!outlet_timers[i][j].repeat)
                    outlet_timers[i][j].status = DISABLED;
                Outlet_Set_State((Outlet_TypeDef) i, outlet_timers[i][j].state);
            }
        }
    }
}