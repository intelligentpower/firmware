/**
 ******************************************************************************
 * @file    powerstrip_api.cpp
 * @author  Logan Anteau
 * @version V1.0.0
 * @date    15-November-2014
 * @brief   API of power strip exposed to Android app
 ******************************************************************************
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "powerstrip_api.h"


/* Function: api_outlet_status(String command)
 *
 * Accepts argument in form of:
 * None
 *
 */
int api_outlet_status(String command)
{
    return Outlet_Get_Status();
}

/* Function: api_toggle_outlet(String command) 
 * 
 * Accepts argument in form of:
 * "<outlet>,<status>"
 *
 * For example: "0,ON", turns on Outlet 1
 */
int api_toggle_outlet(String command)
{
    int loc1;
    Outlet_TypeDef outlet;
    String arg;
    Outlet_State_TypeDef newStatus;

    loc1 = command.indexOf(",");
    outlet = (Outlet_TypeDef) command.substring(0,loc1).toInt();
    arg = command.substring(loc1+1);

    if(arg == "ON")
        newStatus = ON;
    else if(arg == "OFF")
        newStatus = OFF;

    Outlet_Set_State(outlet, newStatus);

    return 0;
}

/* Function: api_set_time(String command)
 * 
 * Accepts argument in form of:
 * "<hours>,<minutes>,<seconds>"
 *
 * For example: "23,43,10", sets the time to 23:43:10
 */
int api_set_time(String command)
{
  int loc1;
  int loc2;
  int loc3;
  int hours;
  int minutes;
  int seconds;
  loc1 = command.indexOf(",");
  hours = command.substring(0, loc1).toInt();

  loc2 = command.indexOf(",", loc1+1);
  minutes = command.substring(loc1+1, loc2).toInt();

  loc3 = command.indexOf(",", loc2+1);
  seconds = command.substring(loc2+1, loc3).toInt();

  int status = set_time(hours, minutes, seconds);

  return status;
}


/* Function: api_set_date(String command)
 * 
 * Accepts argument in form of:
 * "<weekday>,<month>,<day>,<year>"
 *
 * For example: "1,11,10,14", sets the date to Monday, November 10th, 2014
 */
int api_set_date(String command)
{
    int loc1;
    int loc2;
    int loc3;
    int loc4;
    uint8_t weekday;
    uint8_t month;
    uint8_t day;
    uint8_t year;

    loc1 = command.indexOf(",");
    weekday = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    month = command.substring(loc1+1, loc2).toInt();    

    loc3 = command.indexOf(",", loc2+1);
    day = command.substring(loc2+1, loc3).toInt();

    loc4 = command.indexOf(",", loc3+1);
    year = command.substring(loc3+1, loc4).toInt();

    int status = set_date(weekday, month, day, year);

    return status;
}

/* Function: api_add_timer(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<weekday>,<hour>,<minute>,<state>,<status>,<repeat>"
 *
 * For example: "0,4,6,0,ON,ENABLED,TRUE"
 * turns on Outlet 1, at 06:00 that repeats every Wednesday
 */
int api_add_timer(String command)
{
    int loc1;
    int loc2;
    int loc3;
    int loc4;
    int loc5;
    int loc6;
    int loc7;
    uint8_t outlet;
    uint8_t weekday;
    uint8_t hours;
    uint8_t minutes;
    bool repeat;
    String tmp;
    Outlet_State_TypeDef state;
    timer_status status;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    weekday = command.substring(loc1+1, loc2).toInt();

    loc3 = command.indexOf(",", loc2+1);
    hours = command.substring(loc2+1, loc3).toInt();

    loc4 = command.indexOf(",", loc3+1);
    minutes = command.substring(loc3+1, loc4).toInt();

    loc5 = command.indexOf(",", loc4+1);
    tmp = command.substring(loc4+1, loc5);

    if(tmp == "ON")
        state = ON;
    else
        state = OFF;

    loc6 = command.indexOf(",", loc5+1);
    tmp = command.substring(loc5+1, loc6);

    if(tmp == "DISABLED")
        status = DISABLED;
    else
        status = ENABLED;

    loc7 = command.indexOf(",", loc6+1);
    tmp = command.substring(loc6+1, loc7);

    if(tmp == "TRUE")
        repeat = true;
    else
        repeat = false;

    int result = add_timer(outlet, weekday, hours, minutes, state, status,
                            repeat);

    return result;
}

/* Function: api_update_timer(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<timer>,<weekday>,<hour>,<minute>,<state>,<status>,<repeat>"
 *
 * For example: "0,0,4,6,0,ON,ENABLED,TRUE"
 * updates timer 0 on Outlet 1, to 06:00 that repeats every Wednesday
 */
int api_update_timer(String command)
{
    int loc1;
    int loc2;
    int loc3;
    int loc4;
    int loc5;
    int loc6;
    int loc7;
    int loc8;
    uint8_t outlet;
    uint8_t id;
    uint8_t weekday;
    uint8_t hours;
    uint8_t  minutes;
    bool repeat;
    String tmp;
    Outlet_State_TypeDef state;
    timer_status status;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    id = command.substring(loc1+1, loc2).toInt();

    loc3 = command.indexOf(",", loc2+1);
    weekday = command.substring(loc2+1, loc3).toInt();

    loc4 = command.indexOf(",", loc3+1);
    hours = command.substring(loc3+1, loc4).toInt();

    loc5 = command.indexOf(",", loc4+1);
    minutes = command.substring(loc4+1, loc5).toInt();

    loc6 = command.indexOf(",", loc5+1);
    tmp = command.substring(loc5+1, loc6);

    if(tmp == "ON")
        state = ON;
    else
        state = OFF;

    loc7 = command.indexOf(",", loc6+1);
    tmp = command.substring(loc6+1, loc7);

    if(tmp == "DISABLED")
        status = DISABLED;
    else
        status = ENABLED;

    loc8 = command.indexOf(",", loc7+1);
    tmp = command.substring(loc7+1, loc8);

    if(tmp == "TRUE")
        repeat = true;
    else
        repeat = false;

    int result = update_timer(outlet, id, weekday, hours, minutes, state,
                                status, repeat);

    return result;
}

/* Function: api_del_timer(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<timer id>"
 *
 * For example: "2,1", deletes timer index 1 from Outlet 3.
 */
int api_del_timer(String command)
{
    int loc1;
    int loc2;
    uint8_t outlet;
    uint8_t id;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    id = command.substring(loc1+1, loc2).toInt();

    int result = del_timer(outlet, id);
    return result;
}

/* Function: api_disable_timer(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<timer id>"
 *
 * For example: "2,1", disables timer index 1 from Outlet 3.
 */
int api_disable_timer(String command)
{
    int loc1;
    int loc2;
    uint8_t outlet;
    uint8_t id;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    id = command.substring(loc1+1, loc2).toInt();

    int result = disable_timer(outlet, id);
    return result;
}

/* Function: api_enable_timer(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<timer id>"
 *
 * For example: "2,1", enables timer index 1 from Outlet 3.
 */
int api_enable_timer(String command)
{
    int loc1;
    int loc2;
    uint8_t outlet;
    uint8_t id;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    id = command.substring(loc1+1, loc2).toInt();

    int result = enable_timer(outlet, id);
    return result;
}

/* Function: api_set_current_limit(String command)
 *
 * Accepts argument in form of:
 * "<outlet>,<current limit>"
 *
 * For example: "1,2.5", sets a current limit of 2.5 amps on Outlet 2.
 */
int api_set_current_limit(String command)
{
    int loc1;
    int loc2;
    uint8_t outlet;
    float limit;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    loc2 = command.indexOf(",", loc1+1);
    limit = command.substring(loc1+1, loc2).toFloat();

    return Outlet_Set_Limit((Outlet_TypeDef) outlet, limit);
}

/* Function: api_remove_current_limit(String command)
 *
 * Accepts argument in form of:
 * "<outlet>"
 *
 * For example: "1", sets Outlet 2's limit back to the default 15 amps.
 */
int api_remove_current_limit(String command)
{
    int loc1;
    uint8_t outlet;

    loc1 = command.indexOf(",");
    outlet = command.substring(0, loc1).toInt();

    return Outlet_Remove_Limit((Outlet_TypeDef) outlet);
}