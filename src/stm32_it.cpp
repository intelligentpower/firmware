/*
 ******************************************************************************
 * @file    stm32_it.cpp
 * @author  Satish Nair and Logan Anteau
 * @version V1.0.1
 * @date    03-December-2014
 * @brief   Main Interrupt Service Routines.
 *          This file provides template for all exceptions handler and peripherals
 *          interrupt service routine.
 ******************************************************************************
  Copyright (c) 2013 Spark Labs, Inc.  All rights reserved.
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "spark_macros.h"
#include "debug.h"
#include "stm32_it.h"
#include "main.h"
#include "outlet_control.h"
#include <stdio.h>
#include <math.h>

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/
extern __IO uint16_t BUTTON_DEBOUNCED_TIME[];

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M Processor Exceptions Handlers                         */
/******************************************************************************/

/*******************************************************************************
 * Function Name  : NMI_Handler
 * Description    : This function handles NMI exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void NMI_Handler(void)
{
}

/*******************************************************************************
 * Function Name  : HardFault_Handler
 * Description    : This function handles Hard Fault exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void HardFault_Handler(void)
{
    /* Go to infinite loop when Hard Fault exception occurs */
    PANIC(HardFault,"HardFault");
    while (1)
    {
    }
}

/*******************************************************************************
 * Function Name  : MemManage_Handler
 * Description    : This function handles Memory Manage exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void MemManage_Handler(void)
{
    /* Go to infinite loop when Memory Manage exception occurs */
        PANIC(MemManage,"MemManage");
    while (1)
    {
    }
}

/*******************************************************************************
 * Function Name  : BusFault_Handler
 * Description    : This function handles Bus Fault exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void BusFault_Handler(void)
{
    /* Go to infinite loop when Bus Fault exception occurs */
        PANIC(BusFault,"BusFault");
        while (1)
    {
    }
}

/*******************************************************************************
 * Function Name  : UsageFault_Handler
 * Description    : This function handles Usage Fault exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void UsageFault_Handler(void)
{
    /* Go to infinite loop when Usage Fault exception occurs */
        PANIC(UsageFault,"UsageFault");
    while (1)
    {
    }
}

/*******************************************************************************
 * Function Name  : SVC_Handler
 * Description    : This function handles SVCall exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void SVC_Handler(void)
{
}

/*******************************************************************************
 * Function Name  : DebugMon_Handler
 * Description    : This function handles Debug Monitor exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void DebugMon_Handler(void)
{
}

/*******************************************************************************
 * Function Name  : PendSV_Handler
 * Description    : This function handles PendSVC exception.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void PendSV_Handler(void)
{
}

/*******************************************************************************
 * Function Name  : SysTick_Handler
 * Description    : This function handles SysTick Handler.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void SysTick_Handler(void)
{
    System1MsTick();
    Timing_Decrement();
}

/******************************************************************************/
/*                 STM32 Peripherals Interrupt Handlers                       */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32xxx.S).                                                */
/******************************************************************************/
void EXTI9_5_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line6) != RESET)//BUTTON1_EXTI_LINE
    {
        /* Clear the EXTI line pending bit */
        EXTI_ClearITPendingBit(EXTI_Line6);//BUTTON1_EXTI_LINE

        BUTTON_DEBOUNCED_TIME[BUTTON1] = 0x00;

        /* Disable BUTTON1 Interrupt */
        BUTTON_EXTI_Config(BUTTON1, DISABLE);

        /* Enable TIM1 CC4 Interrupt */
        TIM_ITConfig(TIM5, TIM_IT_CC4, ENABLE);

        EXTI_ClearFlag(EXTI_Line6);
    }
    else if(EXTI_GetITStatus(EXTI_Line9) != RESET) // ADC EOC* Line
    {
        EXTI_ClearITPendingBit(EXTI_Line9);

        conversion_complete = 1;
        
        GPIO_ResetBits(ADC_CONVST_PORT, ADC_CONVST_PIN);
    }
}

/*******************************************************************************
 * Function Name  : EXTI15_10_IRQHandler
 * Description    : This function handles EXTI15_10 interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void EXTI15_10_IRQHandler(void)
{
    if (EXTI_GetITStatus(CC3000_WIFI_INT_EXTI_LINE) != RESET)//CC3000_WIFI_INT_EXTI_LINE
    {
        /* Clear the EXTI line pending bit */
        EXTI_ClearITPendingBit(CC3000_WIFI_INT_EXTI_LINE);//CC3000_WIFI_INT_EXTI_LINE

        SPI_EXTI_IntHandler();
    }
}

/*******************************************************************************
 * Function Name  : TIM5_IRQHandler
 * Description    : This function handles TIM5 Capture Compare interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void TIM5_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM5, TIM_IT_CC4) != RESET)
    {
        TIM_ClearITPendingBit(TIM5, TIM_IT_CC4);

        if (BUTTON_GetState(BUTTON1) == BUTTON1_PRESSED)
        {
            BUTTON_DEBOUNCED_TIME[BUTTON1] += BUTTON_DEBOUNCE_INTERVAL;
        }
        else
        {
            /* Disable TIM1 CC4 Interrupt */
            TIM_ITConfig(TIM5, TIM_IT_CC4, DISABLE);

            /* Enable BUTTON1 Interrupt */
            BUTTON_EXTI_Config(BUTTON1, ENABLE);
        }
    }
}

/*******************************************************************************
 * Function Name  : TIM3_IRQHandler
 * Description    : This function handles TIM3 Capture Compare interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void TIM3_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM3, TIM_IT_CC4) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_CC4);
        if(conversion_complete)
        {
            conversion_complete = 0;
            int16_t result;

            /* Drive CS and RD low */
            GPIO_ResetBits(ADC_CS_PORT, ADC_CS_PIN | ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            // result = ADC_Read();
            // result1 = ((float)result / 32768) * 5.0f;


            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result2 = ((float)result / 32768) * 5.0f;
            result2 = (result2 - 2.4836f) * 10.25f; /* 10.25 A/v */
            result2 = result2 * result2;
            if(result2 <= 0.0225f || outlet_states[5] == OFF)
                result2 = 0.0f;
            current5_avg += result2;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result3 = ((float)result / 32768) * 5.0f;
            result3 = (result3 - 2.4734f) * 10.25f; /* 10.25 A/v */
            result3 = result3 * result3;
            if(result3 <= 0.0225f || outlet_states[4] == OFF)
                result3 = 0.0f;
            current4_avg += result3;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result4 = ((float)result / 32768) * 5.0f;
            result4 = (result4 - 2.4708f) * 10.25f; /* 10.25 A/v */
            result4 = result4 * result4;
            if(result4 <= 0.0225f || outlet_states[3] == OFF)
                result4 = 0.0f;
            current3_avg += result4;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result5 = ((float)result / 32768) * 5.0f;
            result5 = (result5 - 2.4762f) * 10.25f; /* 10.25 A/v */
            result5 = result5 * result5;
            if(result5 <= 0.0225f || outlet_states[2] == OFF)
                result5 = 0.0f;
            current2_avg += result5;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result6 = ((float)result / 32768) * 5.0f;
            result6 = (result6 - 2.4782f) * 10.25f; /* 10.25 A/v */
            result6 = result6 * result6;
            if(result6 <= 0.0225f || outlet_states[1] == OFF)
                result6 = 0.0f;
            current1_avg += result6;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result7 = ((float)result / 32768) * 5.0f;
            result7 = (result7 - 2.4785f) * 10.25f; /* 10.25 A/v */
            result7 = result7 * result7;
            if(result7 <= 0.0225f || outlet_states[0] == OFF)
                result7 = 0.0f;
            current0_avg += result7;

            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            GPIO_ResetBits(ADC_RD_PORT, ADC_RD_PIN);
            result = ADC_Read();
            result8 = ((float)result / 32768) * 5.0f;
            result8 = result8 * 67.54f; /* 67.54 V/V */
            result8 = result8 * result8;
            voltage_avg += result8;

            power0_avg += sqrt(result8) * sqrt(result7);
            power1_avg += sqrt(result8) * sqrt(result6);
            power2_avg += sqrt(result8) * sqrt(result5);
            power3_avg += sqrt(result8) * sqrt(result4);
            power4_avg += sqrt(result8) * sqrt(result3);
            power5_avg += sqrt(result8) * sqrt(result2);

            if(counter == NUM_OF_SAMPLES)
            {
                counter = 0;
                /* Disable TIM1 CC4 Interrupt */
                TIM_ITConfig(TIM3, TIM_IT_CC4, DISABLE);
                okay_to_average = 1;
            }
            else
                counter++;

            GPIO_SetBits(ADC_CS_PORT, ADC_CS_PIN | ADC_RD_PIN);

            ADC_StartConversion();
        }

    }
}

/*******************************************************************************
 * Function Name  : TIM2_IRQHandler
 * Description    : This function handles TIM2 global interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        int i;
        for(i = 0; i < OUTLETn; i++)
        {
            if(outlet_states[i] == ON)
            Outlet_GPIO_On(i);
        }
    }
    else if (TIM_GetITStatus(TIM2, TIM_IT_CC1) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
        int i;
        for(i = 0; i < OUTLETn; i++)
        {
            if(outlet_states[i] == ON)
            Outlet_GPIO_Off(i);
        }
    }

}

/*******************************************************************************
 * Function Name  : RTC_WKUP_IRQHandler
 * Description    : This function handles RTC WKUP interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void RTC_WKUP_IRQHandler(void)
{
}

/*******************************************************************************
* Function Name  : RTCAlarm_IRQHandler
* Description    : This function handles RTC Alarm interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void RTCAlarm_IRQHandler(void)
{
}

/*******************************************************************************
 * Function Name  : DMA1_Stream4_IRQHandler
 * Description    : This function handles SPI2_TX_DMA interrupt request.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void DMA1_Stream4_IRQHandler(void)
{
    SPI_DMA_IntHandler();
}
