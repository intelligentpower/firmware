/**
 ******************************************************************************
 * @file    outlet_control.c
 * @author  Logan Anteau
 * @version V1.0.0
 * @date    03-December-2014
 *
 * @brief   Outlet Control Functions
 ******************************************************************************
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "outlet_control.h"
#include "hw_config.h"

Outlet_State_TypeDef outlet_states[OUTLETn];
float current_limits[OUTLETn] = {15.0f, 15.0f, 15.0f, 15.0f, 15.0f, 15.0f};

void Load_Outlets(void)
{
    uint32_t outlet_register = Get_Outlet_Register();
    int i;

    /* If first bit is not set, turn on all outlets. Default State */
    if(!(outlet_register & 0x1))
    {
        for(i = 0; i < OUTLETn; i++)
            Outlet_Set_State(i, ON);
        return;
    }

    for(i = 0; i < OUTLETn; i++)
    {
        if(outlet_register & (1 << i+1))
            Outlet_Set_State(i, ON);
    }
}

uint8_t Outlet_Get_Status(void)
{
    uint8_t status = 0;
    int i;
    for(i = 0; i < OUTLETn; i++)
    {
        if(outlet_states[i] == ON)
            status |= (1 << i);
    }

    return status;
}

void Outlet_Set_State(Outlet_TypeDef outlet, Outlet_State_TypeDef NewState)
{
    uint32_t outlet_register;
    outlet_register = Get_Outlet_Register();

    if(NewState == ON)
    {
        Outlet_GPIO_On(outlet);
        Delay(50); /* Delay onset of PWM */
        outlet_states[outlet] = ON;
        outlet_register |= (1 << outlet + 1);
    }
    else
    {
        outlet_states[outlet] = OFF;
        Outlet_GPIO_Off(outlet);
        outlet_register &= !(1 << outlet + 1);
    }

    /* Always set first bit of outlet_register to 1 */
    outlet_register |= 0x1;

    Set_Outlet_Register(outlet_register);
}

int Outlet_Set_Limit(Outlet_TypeDef outlet, float current_limit)
{
    if(outlet > OUTLETn-1 || outlet < 0)
        return -1;

    if(current_limit > 15.0f)
        current_limit = 15.0f;
    if(current_limit < 0.0f)
        current_limit = 0.0f;

    current_limits[outlet] = current_limit;
    return 0;
}

int Outlet_Remove_Limit(Outlet_TypeDef outlet)
{
    if(outlet > OUTLETn-1 || outlet < 0)
        return -1;
    current_limits[outlet] = 15.0f;
    return 0;
}
