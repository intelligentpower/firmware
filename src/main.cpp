/**
 ******************************************************************************
 * @file    main.cpp
 * @author  Satish Nair, Zachary Crockett, Zach Supalla, Mohit Bhoite and 
                Logan Anteau
 * @version V1.0.1
 * @date    03-December-2014
 * 
 * Updated: 14-Feb-2014 David Sidrane <david_s5@usa.net>
 * 
 * @brief   Main program body.
 ******************************************************************************
  Copyright (c) 2013 Spark Labs, Inc.  All rights reserved.
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */
  
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "debug.h"
#include "spark_utilities.h"
#include "powerstrip_api.h"
#include "spark_wiring_limited.h"

extern "C" {
#include "sst25vf_spi.h"
#include "outlet_timers.h"
#include "math.h"
}

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
volatile uint32_t TimingFlashUpdateTimeout;

int updateTime = 0;
int publishStatus = 0;
int updateTimers = 0;

char status_string[128];

extern uint8_t conversion_complete = 0;
extern float result2 = 0;
extern float result3 = 0;
extern float result4 = 0;
extern float result5 = 0;
extern float result6 = 0;
extern float result7 = 0;
extern float result8 = 0;
extern uint16_t counter = 0;
uint16_t voltage_count = 0;
uint8_t voltage_interval = 0;
uint8_t okay_to_average = 0;

float voltage_avg = 0.0f;
float current0_avg = 0.0f;
float current1_avg = 0.0f;
float current2_avg = 0.0f;
float current3_avg = 0.0f;
float current4_avg = 0.0f;
float current5_avg = 0.0f;
float power0_avg = 0.0f;
float power1_avg = 0.0f;
float power2_avg = 0.0f;
float power3_avg = 0.0f;
float power4_avg = 0.0f;
float power5_avg = 0.0f;

float voltage_final = 0.0f;
float current0_final = 0.0f;
float current1_final = 0.0f;
float current2_final = 0.0f;
float current3_final = 0.0f;
float current4_final = 0.0f;
float current5_final = 0.0f;
float power0_final = 0.0f;
float power1_final = 0.0f;
float power2_final = 0.0f;
float power3_final = 0.0f;
float power4_final = 0.0f;
float power5_final = 0.0f;

static void IntToUnicode (uint32_t value , uint8_t *pbuf , uint8_t len);

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
 * Function Name  : SparkCoreConfig.
 * Description    : Called in startup routine, before calling C++ constructors.
 * Input          : None.
 * Output         : None.
 * Return         : None.
 *******************************************************************************/
extern "C" void SparkCoreConfig(void)
{
    DECLARE_SYS_HEALTH(ENTERED_SparkCoreConfig);
    /* Set the Vector Table(VT) base location at 0xC000 */
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0xC000);
    USE_SYSTEM_FLAGS = 1;

    Set_System();

    SysTick_Configuration();

    /* Enable CRC clock */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);

    LED_SetRGBColor(RGB_COLOR_WHITE);
    LED_SetBrightness(24); /* 25% */
    LED_On(LED_RGB);
    SPARK_LED_FADE = 1;

    /* Configure RTC */
    RTC_Configuration();

    /* Configure Timer for Software PWM of Relays */
    //Relay_Timer_Configure();

    /* Configure outlet GPIO and load states */
    Outlet_GPIO_Init();
    Load_Outlets();

    /* Initialize ADC */
    ADC_Init();
    ADC_Timer_Configure();

    init_timers();

#ifdef IWDG_RESET_ENABLE
    // ToDo this needs rework for new bootloader
    /* Check if the system has resumed from IWDG reset */
    if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
    {
        /* IWDGRST flag set */
        IWDG_SYSTEM_RESET = 1;

        /* Clear reset flags */
        RCC_ClearFlag();
    }

    /* We are duplicating the IWDG call here for compatibility with old bootloader */
    /* Set IWDG Timeout to 3 secs */
    IWDG_Reset_Enable(3 * TIMING_IWDG_RELOAD);
#endif

    Load_SystemFlags();

    sFLASH_Init();

    /* Start Spark Wlan and connect to Wifi Router by default */
    SPARK_WLAN_SETUP = 1;

    /* Connect to Spark Cloud by default */
    SPARK_CLOUD_CONNECT = 1;
}

/*******************************************************************************
 * Function Name  : main.
 * Description    : main routine.
 * Input          : None.
 * Output         : None.
 * Return         : None.
 *******************************************************************************/
int main(void)
{
    // We have running firmware, otherwise we wouldn't have gotten here
    DECLARE_SYS_HEALTH(ENTERED_Main);

    if (SPARK_WLAN_SETUP)
    {
        SPARK_WLAN_Setup(Multicast_Presence_Announcement);
    }

    static uint8_t FIRST_RUN = 1;
    static uint8_t FIRST_CONV = 1;

    /* Main loop */
    while (1)
    {
        if(FIRST_CONV)
        {
            ADC_StartConversion();
            FIRST_CONV = 0;
        }

        if(okay_to_average)
        {
            Spark.process();
            voltage_final = voltage_avg / 2048.0f;
            voltage_final = sqrt(voltage_final);
            current0_final = current0_avg / 2048.0f;
            current0_final = sqrt(current0_final);
            current1_final = current1_avg / 2048.0f;
            current1_final = sqrt(current1_final);
            current2_final = current2_avg / 2048.0f;
            current2_final = sqrt(current2_final);
            Spark.process();
            current3_final = current3_avg / 2048.0f;
            current3_final = sqrt(current3_final);
            current4_final = current4_avg / 2048.0f;
            current4_final = sqrt(current4_final);
            current5_final = current5_avg / 2048.0f;
            current5_final = sqrt(current5_final);
            power0_final = power0_avg / 2048.0f;
            power1_final = power1_avg / 2048.0f;
            power2_final = power2_avg / 2048.0f;
            power3_final = power3_avg / 2048.0f;
            power4_final = power4_avg / 2048.0f;
            power5_final = power5_avg / 2048.0f;
            Spark.process();

            voltage_avg = 0.0f;
            current0_avg = 0.0f;
            current1_avg = 0.0f;
            current2_avg = 0.0f;
            current3_avg = 0.0f;
            current4_avg = 0.0f;
            current5_avg = 0.0f;
            power0_avg = 0.0f;
            power1_avg = 0.0f;
            power2_avg = 0.0f;
            power3_avg = 0.0f;
            power4_avg = 0.0f;
            power5_avg = 0.0f;

            okay_to_average = 0;
            Spark.process();
            TIM_ITConfig(TIM3, TIM_IT_CC4, ENABLE);
        }
        if(SPARK_WLAN_SETUP)
        {
            DECLARE_SYS_HEALTH(ENTERED_WLAN_Loop);
            SPARK_WLAN_Loop();
        }

        if(!SPARK_WLAN_SETUP || SPARK_WLAN_SLEEP || !SPARK_CLOUD_CONNECT || SPARK_CLOUD_CONNECTED)
        {
            if(!SPARK_FLASH_UPDATE && !IWDG_SYSTEM_RESET)
            {
                if(FIRST_RUN)
                {
                    SYSTEM_MODE(AUTOMATIC);

                    // register the Spark function
                    Spark.function("out_toggle", api_toggle_outlet);
                    Spark.function("set_time", api_set_time);
                    Spark.function("set_date", api_set_date);
                    Spark.function("add_timer", api_add_timer);
                    Spark.function("update_timer", api_update_timer);
                    Spark.function("del_timer", api_del_timer);
                    Spark.function("dis_timer", api_disable_timer);
                    Spark.function("en_timer", api_enable_timer);
                    Spark.function("out_status", api_outlet_status);
                    Spark.function("set_curr_lim", api_set_current_limit);
                    Spark.function("del_curr_lim", api_remove_current_limit);
                    FIRST_RUN = 0;
                }

                //Execute main system loop
                DECLARE_SYS_HEALTH(ENTERED_Loop);
                if(publishStatus)
                {
                    float power_values[OUTLETn] = {power0_final, power1_final, 
                        power2_final, power3_final, power4_final, power5_final};
                    float current_values[OUTLETn] = {current0_final, current1_final, 
                        current2_final, current3_final, current4_final, current5_final};
                    for(int i = 0; i < OUTLETn; i++)
                    {
                        if(outlet_states[i] == OFF)
                        {
                            power_values[i] = 0.0f;
                            current_values[i] = 0.0f;
                        }
                        if(current_values[i] > current_limits[i])
                            Outlet_Set_State((Outlet_TypeDef) i, OFF);
                    }
                    
                    sprintf(status_string,
                        "%.1f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f",
                        voltage_final, current_values[0], current_values[1],
                        current_values[2], current_values[3], current_values[4],
                        current_values[5], power_values[0], power_values[1],
                        power_values[2], power_values[3], power_values[4],
                        power_values[5]);
                    // sprintf(status_string,
                    //     "{\"v\": %.1f, \"c0\": %.2f, \"p0\": %.0f, \"c1\": %.2f, \"p1\": %.0f}",
                    //     voltage_final,current0_final,power0_final,current1_final,power1_final);
                    // Spark.publish("status", status_string);
                    // sprintf(status_string,
                    //     "{\"c2\": %.2f, \"p2\": %.0f, \"c3\": %.2f, \"p3\": %.0f}",
                    //     current2_final,power2_final,current3_final,power3_final);
                    // Spark.publish("status2", status_string);
                    // sprintf(status_string,
                    //     "{\"c4\": %.2f, \"p4\": %.0f, \"c5\": %.2f, \"p5\": %.0f}",
                    //     current4_final,power4_final,current5_final,power5_final);
                    // Spark.publish("status3", status_string);
                    publishStatus = 0;
                }
                if(updateTimers)
                {
                    process_timers();
                    updateTimers = 0;
                }
                DECLARE_SYS_HEALTH(RAN_Loop);
            }
        }
    }
}

/*******************************************************************************
 * Function Name  : Timing_Decrement
 * Description    : Decrements the various Timing variables related to SysTick.
 * Input          : None
 * Output         : Timing
 * Return         : None
 *******************************************************************************/
void Timing_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

    if (LED_RGB_OVERRIDE != 0)
    {
        if ((LED_Spark_Signal != 0) && (NULL != LED_Signaling_Override))
        {
            LED_Signaling_Override();
        }
    }
    if (TimingLED != 0x00)
    {
        TimingLED--;
    }
    else if(WLAN_SMART_CONFIG_START || SPARK_FLASH_UPDATE || Spark_Error_Count)
    {
        //Do nothing
    }
    else if(SPARK_LED_FADE)
    {
        LED_Fade(LED_RGB);
        TimingLED = 20;//Breathing frequency kept constant
    }
    else if(SPARK_WLAN_SETUP && SPARK_CLOUD_CONNECTED)
    {
#if defined (RGB_NOTIFICATIONS_CONNECTING_ONLY)
        LED_Off(LED_RGB);
#else
        LED_SetRGBColor(RGB_COLOR_CYAN);
        LED_On(LED_RGB);
        SPARK_LED_FADE = 1;
#endif
    }
    else
    {
        LED_Toggle(LED_RGB);
        if(SPARK_CLOUD_SOCKETED)
            TimingLED = 50;     //50ms
        else
            TimingLED = 100;    //100ms
    }

    if(TimingNTP != 0x00)
    {
        TimingNTP--;
    }
    if(TimingOutletTimers != 0x00)
    {
        TimingOutletTimers--;
    }
    else
    {
        TimingOutletTimers = 5000;
        updateTimers = 1;
    }

    if(TimingOutletStatus != 0x00)
    {
        TimingOutletStatus--;
    }
    else
    {
        TimingOutletStatus = 3000;
        publishStatus = 1;
    }

    if(!SPARK_WLAN_SETUP || SPARK_WLAN_SLEEP)
    {
        //Do nothing
    }
    else if(SPARK_FLASH_UPDATE)
    {
        if (TimingFlashUpdateTimeout >= TIMING_FLASH_UPDATE_TIMEOUT)
        {
            //Reset is the only way now to recover from stuck OTA update
            NVIC_SystemReset();
        }
        else
        {
            TimingFlashUpdateTimeout++;
        }
    }
    else if(!WLAN_SMART_CONFIG_START && BUTTON_GetDebouncedTime(BUTTON1) >= 3000)
    {
        BUTTON_ResetDebouncedState(BUTTON1);

        if(!SPARK_WLAN_SLEEP)
        {
            WiFi.listen();
        }
    }
    else if(BUTTON_GetDebouncedTime(BUTTON1) >= 7000)
    {
        BUTTON_ResetDebouncedState(BUTTON1);

        WLAN_DELETE_PROFILES = 1;
    }

#ifdef IWDG_RESET_ENABLE
    if (TimingIWDGReload >= TIMING_IWDG_RELOAD)
    {
        TimingIWDGReload = 0;

        /* Reload WDG counter */
        KICK_WDT();
        DECLARE_SYS_HEALTH(0xFFFF);
    }
    else
    {
        TimingIWDGReload++;
    }
#endif
}

void USART3Put(uint8_t ch)
{
    while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
    USART_SendData(USART3, (uint8_t) ch);
    //Loop until the end of transmission
    while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
    {
    }
}

void USART3_Init(uint32_t baudrate)
{
    GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
    USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
    //NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

    /* This sequence sets up the TX and RX pins
     * so they work correctly with the USART1 peripheral
     */
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9; // Pins 8 (TX) and 9 (RX) are used
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStruct);

    GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3); //
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3);

    USART_InitStruct.USART_BaudRate = baudrate;
    USART_InitStruct.USART_WordLength = USART_WordLength_8b;
    USART_InitStruct.USART_StopBits = USART_StopBits_1;
    USART_InitStruct.USART_Parity = USART_Parity_No;
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStruct.USART_Mode = USART_Mode_Tx;
    USART_Init(USART3, &USART_InitStruct);

    USART_Cmd(USART3, ENABLE);
}

/*******************************************************************************
 * Function Name  : HexToChar.
 * Description    : Convert Hex 32Bits value into char.
 * Input          : None.
 * Output         : None.
 * Return         : None.
 *******************************************************************************/
static void IntToUnicode (uint32_t value , uint8_t *pbuf , uint8_t len)
{
    uint8_t idx = 0;

    for( idx = 0 ; idx < len ; idx ++)
    {
        if( ((value >> 28)) < 0xA )
        {
            pbuf[ 2* idx] = (value >> 28) + '0';
        }
        else
        {
            pbuf[2* idx] = (value >> 28) + 'A' - 10;
        }

        value = value << 4;

        pbuf[ 2* idx + 1] = 0;
    }
}

#ifdef USE_FULL_ASSERT
/*******************************************************************************
 * Function Name  : assert_failed
 * Description    : Reports the name of the source file and the source line number
 *                  where the assert_param error has occurred.
 * Input          : - file: pointer to the source file name
 *                  - line: assert_param error line source number
 * Output         : None
 * Return         : None
 *******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
