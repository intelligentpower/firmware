# Intelligent Power Strip Firmware

## 1. Download and Install Dependencies 

1. GCC for ARM Cortex processors
2. Make
3. Device Firmware Upgrade Utilities
4. Git


#### 1. GCC for ARM Cortex processors
The Intelligent Power Strip uses an ARM Cortex M4 CPU based microcontroller. All of the code is built around the GNU GCC toolchain offered and maintained by ARM.  

Download and install the latest version from: https://launchpad.net/gcc-arm-embedded

#### 2. Make 
In order to turn your source code into binaries, you will need a tool called `make`. Windows users need to explicitly install `make` on their machines. Make sure you can use it from the terminal window.

Download and install the latest version from: http://gnuwin32.sourceforge.net/packages/make.htm

#### 3. Device Firmware Upgrade Utilities
Install dfu-util 0.7. Mac users can install dfu-util with [Homebrew](http://brew.sh/) or [Macports](http://www.macports.org), Linux users may find it in their package manager, and everyone can get it from http://dfu-util.gnumonks.org/index.html

#### 4. Git

Download and install Git: http://git-scm.com/

## 2. Download and Build Repositories

The entire power strip firmware is organized into three repositories. The main firmware is located under *firmware*, while the supporting libraries are subdivided in to *common-lib* and *spark-communication-lib*.

#### How do we *download* these repositories?
You can access all of the repositories via any git interface or download it directly from the website.

#### How do we *build* these repositories?

Make sure you have downloaded and installed all the required dependencies as mentioned [previously.](#1-download-and-install-dependencies). Note, if you've downloaded or cloned these previously, you'll want to `git pull` or redownload all of them before proceeding.

Open up a terminal window, navigate to the build folder under firmware
(i.e. `cd firmware/build`) and type:
```
    make
```

This will build your main application (`firmware/src/application.cpp`) and required dependencies.

##### Common Errors

* `arm-none-eabi-gcc` and other required gcc/arm binaries not in the PATH.
  Solution: Add the /bin folder to your $PATH (i.e. `export PATH="$PATH:<SOME_GCC_ARM_DIR>/bin`).
  Google "Add binary to PATH" for more details.

* You get `make: *** No targets specified and no makefile found.  Stop.`.
  Solution: `cd firmware/build`.

Please issue a pull request if you come across similar issues/fixes that trip you up.

### Navigating the code base

All of the repositories are sub divided into functional folders:

1. `/src` holds all the source code files
2. `/inc` holds all the header files
3. `/build` holds the makefile and is also the destination for the compiled `.bin` and `.hex` files.

## 4. Flash It!

*Make sure you have the `dfu-util` command installed and available through the command line*

#### Steps:
1. Put the power strip into the DFU mode by holding down the MODE button on the underside and then tapping on the RESET button once. Release the MODE button after you start to see the RGB LED flashing in yellow. It's easy to get this one wrong: Make sure you don't let go of the left button until you see flashing yellow, about 3 seconds after you release the right/RESET button. A flash of white then flashing green can happen when you get this wrong. You want flashing yellow.

2. Open up a terminal window on your computer and type this command to find out if the power strip is indeed being detected correctly. 

   `dfu-util -l`   
   you should get the following in return:
   ```
   Found DFU: [1d50:607f] devnum=0, cfg=1, intf=0, alt=0, name="@Internal Flash  /0x08000000/20*001Ka,108*001Kg" 
   Found DFU: [1d50:607f] devnum=0, cfg=1, intf=0, alt=1, name="@SPI Flash : SST25x/0x00000000/512*04Kg"
   ```

   (Windows users will need to use the Zatig utility to replace the USB driver as described earlier)

3. Now, navigate to the build folder in your firmware repository and use the following command to transfer the *.bin* file into the Core.
   ```
   dfu-util -d 1d50:607f -a 0 -s 0x0800C000:leave -D firmware.bin
   ```

   For example, this is how my terminal looks like:
   ```
D:\Spark\firmware\build [master]> dfu-util -d 1d50:607f -a 0 -s 0x08005000:leave -D firmware.bin
   ```
Upon successful transfer, the Core will automatically reset and start the running the program.

### Protocol Details

#### Outlet Switching:
```
POST /v1/devices/{DEVICE_ID}/out_toggle <outlet number>,<new state>
```
Number is 0 through 5, states are OFF and ON.
Example: "2,ON" turns on Outlet 3.

#### Outlet Status:
```
POST /v1/devices/{DEVICE_ID}/out_status
```
Returns an 8 bit unsigned integer corresponding to which outlets are on.
Bit 0 is outlet 0, bit 1 is outlet 1, and so on.

Example: 7 means outlets 0, 1 and 2 are on.

#### Set date:
```
POST /v1/devices/{DEVICE_ID}/set_date <weekday>,<month>,<day>,<year>
```
Example: "1,11,10,14", sets the date to Monday, November 10th, 2014

#### Set time:
````
POST /v1/devices/{DEVICE_ID}/set_time <hours>,<minutes>,<seconds>
````
Example: "23,43,10", sets the time to 23:43:10

#### Add timer:
````
POST /v1/devices/{DEVICE_ID}/add_timer <outlet>,<weekday>,<hour>,<minute>,<state>,<status>,<repeat>
````
Example: "0,4,6,0,ON,ENABLED,TRUE" turns on Outlet 0, at 06:00 on Wednesday and repeats every Wednesday.

#### Update timer:
````
POST /v1/devices/{DEVICE_ID}/update_timer <outlet>,<timer_num>,<weekday>,<hour>,<minute>,<state>,<status>,<repeat>
````
Example: "0,0,4,6,0,ON,ENABLED,TRUE" updates timer 0 on Outlet 0, to 06:00 on Wednesday and repeats every Wednesday.

#### Delete timer:
````
POST /v1/devices/{DEVICE_ID}/del_timer <outlet>,<timer id>
````
Example: "2,1", deletes the timer with the id number of 1 from Outlet 3.

#### Disable timer:
````
POST /v1/devices/{DEVICE_ID}/dis_timer <outlet>,<timer id>
````
Example: "2,1", disables the timer with the id number of 1 from Outlet 3.

#### Enable timer:
````
POST /v1/devices/{DEVICE_ID}/en_timer <outlet>,<timer id>
````
Example: "2,1", enables the timer with the id number of 1 from Outlet 3.

### VERSION HISTORY

Latest Version: v1.0.0