/**
 ******************************************************************************
 * @file    main.h
 * @author  Satish Nair, Zachary Crockett, Zach Supalla and Mohit Bhoite
 * @version V1.0.0
 * @date    13-March-2013
 * @brief   Header for main.c module
 ******************************************************************************
  Copyright (c) 2013 Spark Labs, Inc.  All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

extern "C" {

/* Includes ------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif
#include "hw_config.h"
#include "spark_wlan.h"
#ifdef __cplusplus
}
#endif

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/*
 * In Eclipse Project Properties -> C/C++ Build -> Settings -> Tool Settings
 * -> ARM Sourcery Windows GCC C/C++ Compiler -> Preprocessor -> Defined symbol (-D),
 * Add : "DFU_BUILD_ENABLE"
 *
 * In Eclipse Project Properties -> C/C++ Build -> Settings -> Tool Settings
 * -> ARM Sourcery Windows GCC C/C++ Linker -> General -> Script file (-T),
 * Browse & select linker file : "linker_stm32f10x_md_dfu.ld"
 */

/*
 * Use the JTAG IOs as standard GPIOs (D3 to D7)
 * Note that once the JTAG IOs are disabled, the connection with the host debugger
 * is lost and cannot be re-established as long as the JTAG IOs remain disabled.
 */
#ifndef USE_SWD_JTAG
#define SWD_JTAG_DISABLE
#endif

/*
 * Use Independent Watchdog to force a system reset when a software error occurs
 * During JTAG program/debug, the Watchdog counting is disabled by debug configuration
 */
#define IWDG_RESET_ENABLE

#define USART_RX_DATA_SIZE			256
#define NUM_OF_SAMPLES          2048

extern uint8_t conversion_complete;
extern float result2;
extern float result3;
extern float result4;
extern float result5;
extern float result6;
extern float result7;
extern float result8;

extern float voltage_avg;
extern float current0_avg;
extern float current1_avg;
extern float current2_avg;
extern float current3_avg;
extern float current4_avg;
extern float current5_avg;
extern float power0_avg;
extern float power1_avg;
extern float power2_avg;
extern float power3_avg;
extern float power4_avg;
extern float power5_avg;

extern float voltage_final;
extern float current0_final;
extern float current1_final;
extern float current2_final;
extern float current3_final;
extern float current4_final;
extern float current5_final;
extern float power0_final;
extern float power1_final;
extern float power2_final;
extern float power3_final;
extern float power4_final;
extern float power5_final;

extern uint8_t okay_to_average;

extern uint16_t counter;
extern uint16_t voltage_count;
extern uint8_t voltage_interval;

/* Exported functions ------------------------------------------------------- */
void Timing_Decrement(void);
void USART3_Init(uint32_t baudrate);
void USART3Put(uint8_t ch);
void USB_USART_Init(uint32_t baudRate);
uint8_t USB_USART_Available_Data(void);
int32_t USB_USART_Receive_Data(void);
void USB_USART_Send_Data(uint8_t Data);
void Handle_USBAsynchXfer(void);
void Get_SerialNum(void);
}

#endif /* __MAIN_H */
