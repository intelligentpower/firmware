/**
  ******************************************************************************
  * @file    outlet_control.h
  * @author  Logan Anteau
  * @version V1.0.0
  * @date    03-December-2014
  * @brief   This file contains all the functions prototypes for the
  *          Outlet Control Functions.
  ******************************************************************************
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OUTLET_CONTROL_H
#define __OUTLET_CONTROL_H

 /* Includes ------------------------------------------------------------------*/
#include "hw_config.h"

#if defined (__cplusplus)
extern "C" {
#endif

/* Exported types ------------------------------------------------------------*/
typedef enum
{
	OFF = 0, ON = 1
} Outlet_State_TypeDef;

extern Outlet_State_TypeDef outlet_states[];
extern float current_limits[];

void Load_Outlets(void);
uint8_t Outlet_Get_Status(void);
void Outlet_Set_State(Outlet_TypeDef outlet, Outlet_State_TypeDef NewState);
void Outlet_Toggle(int outlet);
void Outlet_On(int outlet);
void Outlet_Off(int outlet);
int Outlet_Set_Limit(Outlet_TypeDef outlet, float current_limit);
int Outlet_Remove_Limit(Outlet_TypeDef outlet);

#if defined (__cplusplus)
}
#endif

#endif