#ifndef POWERSTRIP_API_H_
#define POWERSTRIP_API_H_
#include "spark_wiring_limited.h"
#include "outlet_control.h"
#include "outlet_timers.h"

/* Outlet control API functions */
int api_outlet_status(String command);
int api_toggle_outlet(String command);
int api_set_current_limit(String command);
int api_remove_current_limit(String comamnd);

/* Outlet timers API functions */
int api_set_time(String command);
int api_set_date(String command);
int api_add_timer(String command);
int api_update_timer(String command);
int api_del_timer(String command);
int api_disable_timer(String command);
int api_enable_timer(String command);

#endif /* POWERSTRIP_API_H_ */