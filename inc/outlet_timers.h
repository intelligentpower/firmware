/**
  ******************************************************************************
  * @file    outlet_timers.h
  * @author  Logan Anteau
  * @version V1.0.0
  * @date    03-December-2014
  * @brief   This file contains all the functions prototypes for the
  *          Outlet Timer Functions.
  ******************************************************************************
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMERS_H
#define __TIMERS_H
#include "outlet_control.h"

#if defined (__cplusplus)
extern "C" {
#endif

/* Exported macro ------------------------------------------------------------*/

/* Maximum of 10 timers per outlet */
#define MAX_TIMERS      10

/* Exported types ------------------------------------------------------------*/
typedef enum {
    DISABLED,
    ENABLED
} timer_status;

typedef struct {
    uint8_t weekday;
    uint8_t hour;
    uint8_t minute;
    Outlet_State_TypeDef state;
    timer_status status;
    bool repeat;
    uint8_t last_processed;
} timer;

extern timer outlet_timers[OUTLETn][MAX_TIMERS];

void init_timers(void);
void process_timers(void);

int set_time(uint8_t hours, uint8_t minutes, uint8_t seconds);
int set_date(uint8_t weekday, uint8_t month, uint8_t day, uint8_t year);
int add_timer(uint8_t outlet, uint8_t weekday, uint8_t hours, uint8_t minutes,
    Outlet_State_TypeDef state, timer_status status, bool repeat);
int update_timer(uint8_t outlet, uint8_t id, uint8_t weekday, uint8_t hours,
	uint8_t minutes, Outlet_State_TypeDef state, timer_status status, bool repeat);
int del_timer(uint8_t outlet, uint8_t id);
int disable_timer(uint8_t outlet, uint8_t id);
int enable_timer(uint8_t outlet, uint8_t id);

#if defined (__cplusplus)
}
#endif


#endif 